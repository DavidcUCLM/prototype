package Prototype.Prototype;

interface Person {
    Person clone();
}